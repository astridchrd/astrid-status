
from django.shortcuts import render,redirect
from .forms import StatusForm
from .models import Status

def StatusPost(request):
    if request.method == 'POST' :
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('StatusPost')
    else :
        form=StatusForm()

    AllStatus = Status.objects.all()
    args={
        'form':form,
        'AllStatus':AllStatus,
    }
    
    return render(request,'status.html',args)

        

