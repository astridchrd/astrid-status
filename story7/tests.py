from django.test import TestCase,Client
from django.urls import resolve
from . import views
from django.http import HttpRequest

from datetime import datetime
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time



c= Client()

class UnitTestLandingPage(TestCase):
    def test_ada_url_profil(self):
        response = c.get('/story7/')
        self.assertEqual(response.status_code, 200)
    
    def test_story7_pakai_story7_page(self):
        response = c.get('/story7/')
        self.assertTemplateUsed(response, 'story7.html')
    
    def test_story7_panggil_fungsi_views_accordion(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, views.Accordion)
    
    def test_ada_nama(self):
        response= c.get('/story7/')
        case = "Yuk, lebih tau tentang Astrid!" 
        content = response.content.decode("utf8")
        self.assertIn(case, content)

    def test_salah_masukin_url(self):
        response = c.get('/apa')
        self.assertEqual(response.status_code,404)
    
class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_click_and_dark_mode_light_mode(self):
        
        
        # Opening the link we want to test
            selenium = self.browser
            selenium.get('http://127.0.0.1:8000/story7')
            
            # find the element
            acc_1 = selenium.find_element_by_id('ui-id-1')
            acc_2 = selenium.find_element_by_id('ui-id-3')
            acc_3 = selenium.find_element_by_id('ui-id-5')
            

            # test
           
            time.sleep(1)
            acc_2.click()
            time.sleep(1)
            acc_3.click()
            time.sleep(1)
            acc_1.click()
            time.sleep(1)
            
            time.sleep(5)
            e = selenium.find_element_by_css_selector(".toggle")
            time.sleep(2)
            e.click()
            time.sleep(2)

            light = "rgba(255, 255, 255, 1)"
            dark = "rgba(0, 0, 0, 1)"

            pbl = "rgb(255, 251, 242) none repeat scroll 0% 0% / auto padding-box border-box"
            pbn = "rgb(128, 127, 127) none repeat scroll 0% 0% / auto padding-box border-box"

            phl= "rgb(180, 94, 94) none repeat scroll 0% 0% / auto padding-box border-box"
            phn= "rgb(73, 73, 73) none repeat scroll 0% 0% / auto padding-box border-box"

            lh1= "rgba(0, 0, 0, 1)"
            nh1= "rgba(255, 255, 255, 1)"


            bg = selenium.find_element_by_tag_name("body").value_of_css_property("background-color")
            self.assertEqual(bg, dark)

            h1 = selenium.find_element_by_tag_name("h1").value_of_css_property("color")
            self.assertEqual(nh1, h1)

            pb = selenium.find_element_by_class_name("panel-body").value_of_css_property("background")
            self.assertEqual(pb, pbn)

            ph = selenium.find_element_by_class_name("panel-heading").value_of_css_property("background")
            self.assertEqual(phn, ph)

            e.click()
            time.sleep(2)

            bg = selenium.find_element_by_tag_name("body").value_of_css_property("background-color")
            self.assertEqual(bg, light)

            h1 = selenium.find_element_by_tag_name("h1").value_of_css_property("color")
            self.assertEqual(lh1, h1)

            pb = selenium.find_element_by_class_name("panel-body").value_of_css_property("background")
            self.assertEqual(pbl, pb)

            ph = selenium.find_element_by_class_name("panel-heading").value_of_css_property("background")
            self.assertEqual(phl, ph)

            time.sleep(2)

    